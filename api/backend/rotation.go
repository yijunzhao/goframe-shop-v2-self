package backend

import (
	"github.com/gogf/gf/v2/frame/g"
)

// 添加轮播图请求Req
type RotationReq struct {
	g.Meta `path:"/backend/rotation/add" tags:"轮播图" method:"post" summary:"添加轮播图"`
	PicUrl string `json:"pic_url" v:"required#图片链接不能为空" dc:"图片链接"`
	Link   string `json:"link"    v:"required#跳转链接不能为空" dc:"跳转链接"`
	Sort   int    `json:"sort"    dc:"排序"`
}

// 添加轮播图响应Res
type RotationRes struct {
	RotationId int `json:"rotationId"`
}

// 删除轮播图请求Req
type RotationDeleteReq struct {
	g.Meta `path:"/backend/rotation/delete" method:"delete" tags:"轮播图" summary:"删除轮播图"`
	Id     uint `v:"min:1#请选择需要删除的轮播图" dc:"轮播图id"`
}

// 删除轮播图响应Res
type RotationDeleteRes struct{}

// 修改更新轮播图请求Req
type RotationUpdateReq struct {
	g.Meta `path:"/rotation/update/" method:"post" tags:"轮播图" summary:"修改更新轮播图"`
	Id     uint   `json:"id"      v:"min:1#请选择需要修改的轮播图" dc:"轮播图Id"`
	PicUrl string `json:"pic_url" v:"required#轮播图图片链接不能为空" dc:"图片链接"`
	Link   string `json:"link"    v:"required#跳转链接不能为空" dc:"跳转链接"`
	Sort   int    `json:"sort"    dc:"跳转链接"`
}

// 修改更新轮播图响应Res
type RotationUpdateRes struct {
	Id uint `json:"id"`
}

// 查询轮播图列表请求Req
type RotationGetListCommonReq struct {
	g.Meta              `path:"/rotation/list" method:"get" tags:"轮播图" summary:"轮播图列表"`
	Sort                int `json:"sort"   in:"query" dc:"排序类型"`
	CommonPaginationReq     //通用分页查询查询请求Req
}

// 查询轮播图列表响应Res
type RotationGetListCommonRes struct {
	List  interface{} `json:"list" description:"列表"`
	Page  int         `json:"page" description:"分页码"`
	Size  int         `json:"size" description:"分页数量"`
	Total int         `json:"total" description:"数据总数"`
}
