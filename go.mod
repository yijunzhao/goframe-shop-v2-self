module goframe-shop-v2-self

go 1.15

require (
	github.com/gogf/gf/contrib/drivers/mysql/v2 v2.2.3
	github.com/gogf/gf/v2 v2.2.3
	github.com/qiniu/api.v7/v7 v7.8.2
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2
)
