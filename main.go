package main

import (
	_ "goframe-shop-v2-self/internal/packed"

	_ "github.com/gogf/gf/contrib/drivers/mysql/v2"

	_ "goframe-shop-v2-self/internal/logic"

	"github.com/gogf/gf/v2/os/gctx"

	"goframe-shop-v2-self/internal/cmd"
)

func main() {
	cmd.Main.Run(gctx.New())
}
