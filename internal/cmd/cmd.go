package cmd

import (
	"context"
	"goframe-shop-v2-self/internal/consts"
	"goframe-shop-v2-self/internal/controller"
	"goframe-shop-v2-self/internal/controller/backend"
	"goframe-shop-v2-self/internal/controller/hello"

	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gcmd"
)

var (
	Main = gcmd.Command{
		Name:  consts.ProjectName,
		Usage: consts.ProjectUsage,
		Brief: consts.ProjectBrief,
		Func: func(ctx context.Context, parser *gcmd.Parser) (err error) {
			s := g.Server()
			s.Group("/", func(group *ghttp.RouterGroup) {
				//默认中间件，统一响应字段中间件
				/*
					{
					    "code": 0,
					    "message": "",
					    "data": {
					        "xxx":xxx,
							"xxx":xxx,
					    }
					}
				*/
				group.Middleware(ghttp.MiddlewareHandlerResponse)
				//分组绑定路由
				group.Bind(
					hello.New(),
					controller.Rotation, //轮播图
					backend.Article,     //文章
				)
			})
			s.Run()
			s.SetPort(8000)
			return nil
		},
	}
)
