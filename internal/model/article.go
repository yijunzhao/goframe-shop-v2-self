package model

import (
	"goframe-shop-v2-self/internal/model/entity"
)

// ArticleCreateUpdateBase 创建/修改文章基类
type ArticleCreateUpdateBase struct {
	UserId  int
	Title   string
	Desc    string
	PicUrl  string
	IsAdmin int
	Praise  int
	Detail  string
}

// ArticleCreateInput 创建文章输入Input
type ArticleCreateInput struct {
	ArticleCreateUpdateBase
}

// ArticleCreateOutput 创建文章返回结果Output
type ArticleCreateOutput struct {
	Id uint `json:"id"`
}

// ArticleUpdateInput 修改更新文章输入Input
type ArticleUpdateInput struct {
	ArticleCreateUpdateBase
	Id uint
}

// ArticleGetListInput 查询文章列表输入Input
type ArticleGetListInput struct {
	Page int // 分页号码
	Size int // 分页数量，最大50
	Sort int // 排序类型(0:最新, 默认。1:活跃, 2:热度)
	ArticleUserAction
}

// ArticleGetListOutput 查询文章列表输出Output
type ArticleGetListOutput struct {
	List  []ArticleGetListOutputItem `json:"list" description:"列表"`
	Page  int                        `json:"page" description:"分页码"`
	Size  int                        `json:"size" description:"分页数量"`
	Total int                        `json:"total" description:"数据总数"`
}
type ArticleGetListOutputItem struct {
	entity.ArticleInfo
}

// 文章详情输入Input
type ArticleDetailInput struct {
	Id uint
}

// 文章详情输出Output
type ArticleDetailOutput struct {
	entity.ArticleInfo
}

// 删除文章输入Input
type ArticleDeleteInput struct {
	Id uint
	ArticleUserAction
}

// 获取用户相关
type ArticleUserAction struct {
	UserId  int // 用户
	IsAdmin int // 是否是管理员
}
